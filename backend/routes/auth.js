const express = require('express')
const router = require('express-promise-router')();
const passport = require('passport')
const passportConf = require('../config/passport')

const { validateBody, schemas } = require('./helpers/routeValidation')
const UsersConroller = require('../controllers/userController')

const passportLocal = passport.authenticate('local', { session: false })
const jwt = require('jsonwebtoken');

// Register route
router.post("/register", validateBody(schemas.authSchema), UsersConroller.signUp);

// Login route
router.post('/login', UsersConroller.signIn);


module.exports = router;