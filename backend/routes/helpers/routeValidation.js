const Joi = require('joi');

/* Regex for strong password
    requirements:
    - contains at least 1 lowercase letter
    - contains at least 1 uppercase letter
    - contains at least 1 number
    - contains at least 1 special character
    - Must be at least 8 characters long 
*/
const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");


module.exports = {
  validateBody: (schema) => {
    return (req, res, next) => {
      // Validate request body using selected validation schema
      const result = Joi.validate(req.body, schema, { abortEarly: false });

      // If request contains errors then return response with errors
      if (result.error) {
        return res.status(400).json({ message: result.error.details.map(d => d.message), success: false });
      }
      // If validation passes we can continue and return validated values inside request
      if (!req.value) {
        req.value = {}
      };
      req.value['body'] = result.value;
      next();
    }

  },
  schemas: {
    // Validation schema for authentications
    authSchema: Joi.object().keys({
      // username must be a alphanumeric string between 3 and 30 characters
      username: Joi.string().alphanum().min(3).max(30).required(),
      // Password must fulfill strong password requirements
      password: Joi.string().min(6).max(20)
    }),
    // Validation schema for todolist items
    listSchema: Joi.object().keys({
      // Task must be alphanumeric and is required
      task: Joi.string().alphanum().required()
    })
  }
}