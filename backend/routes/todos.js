'use strict'

var express = require('express');
var todoRouter = require('express-promise-router')();
const passport = require('passport')
const passportConf = require('../config/passport')

const listConroller = require('../controllers/listController');
const { validateBody, schemas } = require('./helpers/routeValidation');
const passportJWT = passport.authenticate('jwt', { session: false })

function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ')
    req.token = bearer[1];
    next();
  } else {
    res.sendStatus(403);
  }
}

// get all todo items
todoRouter.route('/').get(passportJWT, verifyToken, listConroller.getItems);

// create new todo item
todoRouter.route("/").post(passportJWT, listConroller.postItem);

// delete a todo item
todoRouter.delete('/:id', passportJWT, listConroller.deleteItem);

// Update a todo item
todoRouter.put("/:id", passportJWT, listConroller.updateItem);

module.exports = todoRouter;