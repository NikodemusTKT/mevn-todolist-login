'use strict'
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var TodoSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  done: {
    type: Boolean,
    default: false
  },
  updateTime: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User"
  }
});

module.exports = mongoose.model('Todo', TodoSchema);