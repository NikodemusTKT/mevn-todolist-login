'use strict'

var express = require('express');
var cors = require('cors');
var morgan = require('morgan');
var path = require('path');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');

var todoRouter = require('./routes/todos');
var authRouter = require('./routes/auth');

// Database connection
var config = require('./Config');
mongoose.Promise = require('bluebird');
 mongoose.connect(process.env.DB || "mongodb://localhost:27017/todolist", { 
   useNewUrlParser: true,
   autoReconnect: true
 }).then(console.log("Connection to the MongoDB successful"))


var app = express();

// Middlewares

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Static public folder for static elements
app.use(express.static(path.join(__dirname, 'public')));

// Express server
const port = config.APP_PORT;
app.listen(port, () => {
  console.log(`Express listening on PORT ${port}`);
});

// Setup cors for access errors
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// Routes
// Use api route for handling todo items
// const passportJWT = passport.authenticate('jwt', { session: false })
app.use('/api/todos',  todoRouter);
app.use('/api/auth', authRouter);

// Serve index.html from the root request
app.get("/", (req, res, next) => {
  res.sendFile('./public/index.html');
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// restful api error handler
app.use(function(err, req, res, next) {
  console.log(err);

  if (req.app.get('env') !== 'development') {
    delete err.stack;
  }

  res.status(err.statusCode || 500).json(err);
});

module.exports = app;