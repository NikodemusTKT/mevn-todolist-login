const User = require('../models/User')
const JWT = require('jsonwebtoken');
const settings = require('../config/settings')
const passport = require('passport');
const passportConf = require('../config/passport');

module.exports = {
  signUp: async (req, res, next) => {
    var { username, password } = req.value.body;
    if (!username || !password) {
      res.json({ success: false, message: 'Please pass username and password.' });
    } else {
      let matchUser = await User.findOne({ username })
      if (matchUser) {
        return res.status(403).json({ success: false, message: "Username already exists" })
      }
      var newUser = new User({ username, password });
      // save the user
      await newUser.save(function(err) {
        if (err) {
          return next(err);
        }
        res.json({ success: true, message: 'Successfully created a new user.' });
      });
    }
  },
  signIn: async (req, res, next) => {
    User.findOne({
      username: req.body.username
    }, function(err, user) {
      if (err) {
        throw err;
      }

      if (!user) {
        return res.status(401).send({ success: false, msg: 'Authentication failed. User not found.' });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var body = { _id: user._id, username: user.username }
            var token = JWT.sign(body, settings.secret);
            // return the information including token as JSON
            res.json({ success: true, token: 'JWT ' + token });
          } else {
            return res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' });
          }
        });
      }
    });

  }
}