const Todo = require('../models/Todo');
const mongoose = require('mongoose')

module.exports = {
  getItems: async (req, res, next) => {
    // Get all todo items from database using userid
    const todos = await Todo.find({ user: req.user._id });
    // If no todos is found respond with 404 message
    if (!todos) {
      return res.status(404).json({
        success: false,
        message: "No any todo items found."
      });
    }
    // Respond with todos
    res.json(todos);
  },
  postItem: async (req, res, next) => {
    let { name, done } = req.body;
    let user = req.user._id;
    let newItem = new Todo({ name, done, user });
    await newItem.save();
    // Respond with 200 and new item data on success
    res.status(200).json({ newItem, success: true });

  },
  deleteItem: async (req, res, next) => {
    //   delete item with request id
    let todo = await Todo.findByIdAndDelete(req.params.id);
    // if no todo item is found with given id, throw error
    if (!todo) {
      return next(new Error('Todo was not found'));
    } else {
      // Respond with 204 status on successful delete
      res.status(204).json({
        success: true,
        message: 'Todo successfully deleted',
        id: todo._id
      });
    }
  },
  updateItem: async (req, res, next) => {
    //   Update todo item
    let done = req.body.done;
    let todo = await Todo.findByIdAndUpdate(req.params.id, { done: done }, {
      new: true
    });
    // If update failed respond with 400 status
    if (!todo) {
      return res.status(400).json({ success: false, message: "Failed to update item" });
    } else {
      // on successful update respond with 200 status and todo data
      res.status(200).json({ success: true, todo });

    }
  }
}