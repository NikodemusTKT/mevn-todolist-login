module.exports = {
  DB: process.env.DB || 'mongodb://localhost:27017/todolist',
  APP_PORT: process.env.API_PORT || 4000
};