var passport = require('passport')
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var LocalStrategy = require('passport-local').Strategy;

// load up the user model
var User = require('../models/User');
var settings = require('../config/settings'); // get settings file

// JWT Authentication strategy
passport.use(new JwtStrategy({

  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
  secretOrKey: settings.secret
}, async (payload, done) => {
  try {
    // find user specified in token
    const user = await User.findById(payload._id);
    if (!user) {
      // If no user found return nothing
      return done(null, false, { message: "No user found" });
    }
    // Otherwise return user
    done(null, user)
  } catch (error) {
    // return error
    done(error, false)
  }
}))

// LocalStrategy for login
// passport.use(new LocalStrategy )