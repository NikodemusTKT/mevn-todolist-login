FROM node:10
WORKDIR /usr/src/app
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .
EXPOSE 4000
ENV NODE_ENV development
CMD npm run build && npm start