module.exports = {
  dev: {
    proxyTable: {
      '/api': {
        target: 'http://localhost:4000',
        changeOrigin: true
      }
    }

  }
}