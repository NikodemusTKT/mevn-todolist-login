import { createLocalVue, shallowMount } from "@vue/test-utils";
import shoppingList from "../components/list/shoppingList.vue";
import faker from "faker";
jest.mock("axios");

const stubItem = () => {
  return {
    _id: faker.random.number(),
    name: faker.commerce.productName(),
    done: false
  };
};

const stubItems = (num = 10) => {
  return Array.from({ length: num }, stubItem);
};
var items = stubItems(10);

describe("Test Shoplist", () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = shallowMount(shoppingList);
    wrapper.setData({ shopItems: items });
  });
  it("is A vue instance", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
  it("is listing items", () => {
    expect(wrapper.vm.shopItems).toEqual(items);
  });
  it("is listing bought items when bought filter is set", () => {
    wrapper.vm.shopItems[2].done = true;
    wrapper.vm.shopItems[4].done = true;
    let boughtItems = [wrapper.vm.shopItems[2], wrapper.vm.shopItems[4]];
    wrapper.vm.shopFilter = "bought";
    expect(wrapper.vm.filteredItems).toEqual(boughtItems);
  });
});
