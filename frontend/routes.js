import shoppingList from './components/list/shoppingList.vue';
import Login from './components/user/Login.vue';
import Register from './components/user/Register.vue';
import Navbar from './components/user/Navbar.vue';

export const routes = [{
    path: '/list',
    component: shoppingList
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  {
    path: '*',
    component: Login
  },
]