const path = require('path')
module.exports {
  outputDir: path.resolve('./backend/public/dist')
configureWebpack: config => {
  if(process.env.NODE_ENV === 'production') {
    config.output.path = path.resolve(__dirname, './backend/public/dist');
    config.output.publicPath = 'dist';
    config.output.filename = 'build.js';
}
    

}