import axios from 'axios';
const url = process.env.API_URL || 'http://localhost:4000/api/todos/'
const authURL = process.env.AUTH_URL || 'http://localhost:4000/api/auth'

export default {
  async setHeaders() {
    axios.defaults.headers.common["Authorization"] = localStorage.getItem(
      "jwtToken"
    );
  },
  async fetchTodos() {
    return await axios.get(url);
  },
  async addTodo(todo) {
    return await axios.post(url, { name: todo, done: false });
  },
  async deleteTodo(id) {
    return await axios.delete(url + id)
  },
  async updateTodo(params) {
    return await axios.put(url + params.id, params);
  },
  async register(params) {
    return await axios.post(authURL + '/register', params);
  },
  async login(params) {
    return await axios.post(authURL + '/login', params);
  }
}