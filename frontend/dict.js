export const dict = {
  custom: {
    password: {
      required: 'Your password is empty'
    },
    username: {
      required: 'Your username is empty'
    },
    confirmPassword: {
      required: "Your confirmation password is empty",
      is: "Confirmation password doesn't match given password"
    }
  }
};
