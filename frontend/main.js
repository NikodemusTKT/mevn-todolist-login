import Vue from 'vue'

import VueRouter from 'vue-router'
import App from './App.vue'
import {
  routes
} from './routes'
import '../node_modules/materialize-css/dist/js/materialize.min.js'
import VeeValidate from 'vee-validate';
import {
  Validator
} from 'vee-validate';
import {
  dict
} from './dict';

Validator.localize('en', dict);

export const eventBus = new Vue();

Vue.use(VueRouter);
Vue.use(VeeValidate);

const router = new VueRouter({
  routes,
  mode: 'history'
});


new Vue({
  router,
  el: '#app',
  render: h => h(App)
})